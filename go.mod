module clean-utility

go 1.19

require (
	github.com/tidwall/gjson v1.14.4
	github.com/yanzay/tbot/v2 v2.2.0
	golang.org/x/sys v0.3.0
)

require (
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
)
